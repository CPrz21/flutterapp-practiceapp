import 'package:flutter/material.dart';

import 'package:practice/src/routes/routes.dart';
import 'package:practice/src/pages/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Practice App',
      initialRoute: '/',
      routes: getApplicationRoutes(),
      onGenerateRoute: (settings) =>
          MaterialPageRoute(builder: (context) => DraggableCardPage()),
      theme: ThemeData(
          brightness: Brightness.dark, //para el fondo de la aplicación
          primaryColor: Color.fromRGBO(7, 89, 157, 1.0),
          accentColor: Colors.cyan[600],
          fontFamily: 'Dosis',
          textTheme: TextTheme(
              headline1:
                  TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold))),
    );
  }
}

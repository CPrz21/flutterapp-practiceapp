import 'package:flutter/material.dart';

class FadeInAndOut extends StatelessWidget {
  final bool visible;
  FadeInAndOut({this.visible});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedOpacity(
        opacity: visible ? 1.0 : 0.0,
        duration: Duration(milliseconds: 500),
        child: Container(
          width: 200.0,
          height: 200.0,
          color: Colors.green,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class SnackBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: RaisedButton(
            child: Text('Show Snackbar'),
            onPressed: () {
              final snackBar = SnackBar(
                content: Text('Snackbar!!!'),
                // duration: Duration(seconds: 1),
                action: SnackBarAction(label: 'Undo', onPressed: () {}),
              );

              Scaffold.of(context).showSnackBar(snackBar);
            }));
  }
}

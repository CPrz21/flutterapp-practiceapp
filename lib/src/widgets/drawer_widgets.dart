import 'package:flutter/material.dart';

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'Drawer Header',
              style: TextStyle(color: Colors.white),
            ),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor),
          ),
          ListTile(
            title: Text('Home'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/');
            },
          ),
          Divider(),
          ListTile(
            title: Text('Fade in and out'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, 'FadeInAndOut');
            },
          ),
          Divider(),
          ListTile(
            title: Text('SnackBar'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, 'SnackBar');
            },
          ),
          Divider(),
          ListTile(
            title: Text('Google Map'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, 'Map');
            },
          ),
        ],
      ),
    );
  }
}

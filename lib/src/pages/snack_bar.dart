import 'package:flutter/material.dart';

import 'package:practice/src/widgets/snack_bar_widget.dart';

class SnackBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Snack Bar'),
      ),
      body: SnackBarWidget(),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:practice/src/widgets/drawer_widgets.dart';
import 'package:practice/src/widgets/fade_in_and_out_widget.dart';

class FadeInAndOutPage extends StatefulWidget {
  @override
  _FadeInAndOutPageState createState() => _FadeInAndOutPageState();
}

class _FadeInAndOutPageState extends State<FadeInAndOutPage> {
  bool _visible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fade in and out'),
      ),
      drawer: DrawerWidget(),
      body: FadeInAndOut(
        visible: _visible,
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.flip),
          tooltip: 'Toggle Opacity',
          onPressed: () {
            setState(() {
              _visible = !_visible;
            });
          }),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:practice/src/widgets/drawer_widgets.dart';
import 'package:practice/src/widgets/draggable_card_widget.dart';

class DraggableCardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Draggable Card'),
      ),
      drawer: DrawerWidget(),
      body: DraggableCard(
        child: FlutterLogo(
          size: 128,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:practice/src/widgets/drawer_widgets.dart';
import 'package:practice/src/widgets/google_map_widget.dart';

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Map Widget'),
      ),
      drawer: DrawerWidget(),
      body: GoogleMapWidget(),
    );
  }
}

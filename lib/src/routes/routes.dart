import 'package:flutter/material.dart';

import 'package:practice/src/pages/home.dart';
import 'package:practice/src/pages/fade_in_and_out_page.dart';
import 'package:practice/src/pages/snack_bar.dart';
import 'package:practice/src/pages/map_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => DraggableCardPage(),
    'FadeInAndOut': (BuildContext context) => FadeInAndOutPage(),
    'SnackBar': (BuildContext context) => SnackBarPage(),
    'Map': (BuildContext context) => MapPage(),
  };
}
